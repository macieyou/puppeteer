const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();
  await page.goto('https://www.morele.net/login', { waitUntil: 'networkidle0' });
  await page.click('.close-cookie-box')
  await page.type('input#username', 'sadowm1990@gmail.com')
  await page.type('input#password-log', 'M@nkey123')

  // click and wait for navigation
  await Promise.all([
    page.click('button[type=submit].btn.btn-primary-2'),
    page.waitForNavigation({ waitUntil: 'networkidle0' }),
  ]);

  await page.goto('https://www.morele.net/lego-ideas-rakieta-nasa-apollo-saturn-v-92176-6279800/', { waitUntil: 'networkidle0' }),
  
  await Promise.all([
    page.click('.add-to-cart__btn'),
    page.waitForSelector('.btn.btn-primary.btn-add-to-basket.page-title-add-to-basket.pushAddToBasketData')
  ]);

  await page.goto('https://www.morele.net/koszyk/', { waitUntil: 'networkidle0' }),

  await Promise.all([
    page.click('.confirm-button'),
    page.waitForSelector('.md-overlay')
  ]);

  await page.goto('https://www.morele.net/dostawa-i-platnosc/', { waitUntil: 'networkidle0' }),

  await Promise.all([
    page.waitForSelector('.circle-loading-page.hidden'),
    page.click("[data-address-id='8365269']"),
    page.waitForSelector('.circle-loading-page.hidden'),
  ]);

  await Promise.all([
    page.click("[data-name='Kurier pobranie 48h - Poczta Polska ']"),
    page.waitForSelector('.circle-loading-page.hidden'),
  ]);

  await page.goto('https://www.morele.net/potwierdzenie/', { waitUntil: 'networkidle0' }),

  await Promise.all([
    page.click("#reg_accept"),
    page.waitForSelector('.form-control-input.checkbox-container.mv-valid'),
  ]);

  await Promise.all([
    page.click(".confirm-button"),
    page.waitForNavigation({ waitUntil: 'networkidle0' }),
  ]);

  await page.screenshot({ path: 'example.png' });

  await browser.close();
  
})();